<?php
require 'databaseAccess.php';
//Start Login Process 
// Use a prepared statement
if(isset($_POST['Login'])){
    $stmt = $mysqli->prepare("SELECT COUNT(*), username, password FROM users WHERE username=?");
    
    // Bind the parameter
    $stmt->bind_param('s', $user);
    $user = $_POST['username'];
    $stmt->execute();
    
    // Bind the results
    $stmt->bind_result($cnt, $user_id, $pass_encrypted);
    $stmt->fetch();
    
    $pass_guess = $_POST['passwordEntered'];
    // Compare the submitted password to the actual password hash
    
    if( $cnt == 1 && crypt($pass_guess,$pass_encrypted)==$pass_encrypted){
                    // Login succeeded!
                    session_start();
                    $_SESSION['user_id'] = $user_id;
                    $_SESSION['token'] = substr(md5(rand()), 0, 10);
                    $_SESSION['username'] = $user;
                    // Redirect to your target page
                    echo 'This worked.';
                    echo $user_id;
                    header("location: userhome.php");
    }else{
                    // Login failed; redirect back to the login screen
                    echo "Login failed. Please try again.";
                    echo "<script>setTimeout(\"location.href = 'index.php';\",1500);</script>";
    }
}
if(isset($_POST['createUser'])){
	//create username and escape input
	if(!empty($_POST['newUserName'])){
		$user = $mysqli->real_escape_string($_POST['newUserName']);
        }else{
			echo 'Error: Make sure to fill in a username.';
		}
	//create password and escape input
    if(!empty($_POST['newPassword'])){
		$pass = $mysqli->real_escape_string($_POST['newPassword']);
        $passEncrypted = crypt($pass,'$1$WQvMDFgI$5.mVOS7V2Q/aB78Mxl13Q1');
       }else{
			echo 'Error: Make sure to fill in a password.';
	}
	//creat first name and escape input
	if(!empty($_POST['firstName'])){
		$first = $mysqli->real_escape_string($_POST['firstName']);
       }else{
			echo 'Error: Make sure to fill in a first name.';
	}
	//create last name and escape input
	if(!empty($_POST['lastName'])){
		$last = $mysqli->real_escape_string($_POST['lastName']);
       }else{
			echo 'Error: Make sure to fill in a last name.';
	}
	//create email and escape input
	if(!empty($_POST['email'])){
		$email = $mysqli->real_escape_string($_POST['email']);;
       }else{
			echo 'Error: Make sure to fill in an email.';
	}
	//create picture link and escape input
	if(!empty($_FILES['picture'])){
        $allowedExt = array('jpeg','jpg','png','gif');
        $file_name = $_FILES['picture']['name'];
        $file_ext = strtolower(end(explode('.', $file_name)));
        $file_temp = $_FILES['picture']['tmp_name'];
        if(in_array($file_ext,$allowedExt)){
            //upload picture
            $file_path = 'imagesfinal/'.substr(crypt(time()), 0, 10) . '.' .$file_ext;
            move_uploaded_file($file_temp, $file_path);
        }else{
            echo 'Incorrect file type. Only PNG, jpeg, jpg, and gif are allowed.';
        }
       }else{
			echo 'Error: Make sure to upload a picture.';
	}
	//prepare and insert user into database
	$stmt = $mysqli->prepare("insert into users (username, password, first_name, last_name, picture_link, email) values (?,?,?,?,?,?)");
	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}else{
	//bind user
        $bind = $stmt->bind_param('ssssss', $user, $passEncrypted, $first, $last, $file_path, $email);
        $execute = $stmt->execute();
        if(!$bind){
            printf("Bind Failed: %s\n", $mysqli->error);
            exit;
        }elseif(!$execute){
            printf("Execute Failed: %s\n", $mysqli->error);
            exit;
        }else{
            echo 'Success';
            $stmt->close();
        }    
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <link href="news_css.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Cinzel" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">
    <title>The Big Bend Bulletin</title>
    <script src="jquery-3.1.1.min.js"></script>
    <script src="register.js"></script>
    <script>
    $(document).ready(function(){
       $('#signUp').click(function(){
            $('#register').fadeIn(400);
            $('body').append('<div class="mask" id="mask" ></div>');
            $('#mask').fadeIn(400);
        });
       $('#closeReg').click(function(){	
		$('#mask, .popupInfo').fadeOut(400, function(){
			$('#mask').remove();
		});
		location.reload();
        });
       $('#signIn').click(function(){
            $('#login').fadeIn(400);
            $('body').append('<div class="mask" id="mask" ></div>');
            $('#mask').fadeIn(400);
        });
       $('#closeSignIn').click(function(){	
		$('#mask, .popupInfo').fadeOut(400, function(){
			$('#mask').remove();
		});
		location.reload();
        });
    }); 
    </script>
</head>

<body>

<div class="header">
    
    <div class="searchDiv">
        <form action = 'articlesearch.php' method = 'get' name = "search">
        <input type="text" class="tField" placeholder="Search BBB..." name = "keywords"/><input type="submit" class="signInSignUpButton" name = "search"/>
        </form>
    </div>
    <div class="signInRegisterDiv">
      <input type="button" class="signInSignUpButton" value="Register" id="signUp" /><input type="button" class="signInSignUpButton" value="Sign In" id="signIn" />
    </div>
</div>
<div class="title">
    The Big Bend Bulletin
    <?php
        date_default_timezone_set('America/Chicago');
        $today = date("l, F j, Y");
        htmlentities(printf("<p class = 'todaydate'>%s</p>",
                            $today));
    ?>
</div>

<div class="container">
    <div id = "register" class="popupInfo">
        <form method="post" name="createUser" enctype = "multipart/form-data">
        <table class="table">
            <tr><td colspan=2><h1 class="titleSmall">Register</h1></td></tr>
            <tr><td>User Name</td><td><input type="text" name = "newUserName" class = "tField" /></td></tr>
            <tr><td>Password</td><td><input type="password" name = "newPassword" class = "tField" /></td></tr>
            <tr><td>First Name</td><td><input type="text" name = "firstName" class = "tField" /></td></tr>
            <tr><td>Last Name</td><td><input type="text" name = "lastName" class = "tField" /></td></tr>
            <tr><td>Email</td><td><input type="text" name = "email" class = "tField" /></td></tr>
            <tr><td>Picture</td><td><input type="file" name = "picture" /></td></tr>
            <tr><td><input type="submit" name="createUser" class="signInSignUpButton" value="Register" /> </td>
            <td><input type="button" id ="closeReg" class="signInSignUpButton" value="Close" /></td></tr>
        </table>
        </form>
    </div>
    <div id = "login" class="popupInfo">
        <form method="post" name="createUser">
        <table class="table">
            <tr><td colspan=2><h1 class="titleSmall">Sign In</h1></td></tr>
            <tr><td>User Name</td><td><input type="text" name = "username" class = "tField" /></td></tr>
            <tr><td>Password</td><td><input type="password" name = "passwordEntered" class = "tField" /></td></tr>
            <tr><td><input type="submit" name="Login" class="signInSignUpButton" value="Login" /> </td>
            <td><input type="button" id ="closeSignIn" class="signInSignUpButton" value="Close" /></td></tr>
        </table>
        </form>
    </div>

    <?php
	$articleid = $_GET['article_id'];
    $stmt = $mysqli->prepare("SELECT * FROM articles WHERE article_id = '$articleid'");
    if(!$stmt){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }
    $stmt->execute();
    $stmt->bind_result($timestamp, $article_id, $posted_by_id, $posted_by_user, $article_title, $description, $link);
    
    while($stmt->fetch()){
            printf("<h1 class = \"articleTitle\">%s</h1><p class=\"articleInfo\">%s<br>Posted By: %s on %s<br><br><a href = \"%s\">%s</a></p>\n",
            htmlspecialchars($article_title),
            htmlspecialchars($description),
            htmlspecialchars($posted_by_user),
            htmlspecialchars($timestamp),
			htmlspecialchars($link),
			htmlspecialchars($link));
    }
    
    $stmt->close();
	?>
	<h1 class="articleTitle">Comments:</h1>
	<?php
	$stmtComment = $mysqli->prepare("SELECT * FROM comments WHERE article_id = '$articleid'");
    if(!$stmtComment){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }
    $stmtComment->execute();
    $stmtComment->bind_result($timestamp, $comment_id, $posted_by_id, $posted_by_user, $article_id,$content);
    
    while($stmtComment->fetch()){
            printf("<p class=\"comments\"><b>%s</b> %s<br><span class =\"commentTimestamp\">Posted On: %s</span></p>\n",
            htmlspecialchars($posted_by_user),
            htmlspecialchars($content),
            htmlspecialchars($timestamp));
    }
	
	
    ?>
</div>



</body>
</html>
