<?php
require 'databaseAccess.php';
//Start Login Process 
// Use a prepared statement
session_start();
if(isset($_SESSION['user_id'])){
}else{
	header('Location: index.php');
}
if(isset($_POST['signout'])){
	session_destroy();
	header('Location: index.php');
}	

?>
<!DOCTYPE html>
<html>
<head>
    <link href="news_css.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Cinzel" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">
    <title>The Big Bend Bulletin</title>
    <script src="jquery-3.1.1.min.js"></script>
    <script src="register.js"></script>
    <script>
    $(document).ready(function(){
       $('#signUp').click(function(){
            $('#register').fadeIn(400);
            $('body').append('<div class="mask" id="mask" ></div>');
            $('#mask').fadeIn(400);
        });
       $('#closeReg').click(function(){	
		$('#mask, .popupInfo').fadeOut(400, function(){
			$('#mask').remove();
		});
		location.reload();
        });
       $('#signIn').click(function(){
            $('#login').fadeIn(400);
            $('body').append('<div class="mask" id="mask" ></div>');
            $('#mask').fadeIn(400);
        });
       $('#closeSignIn').click(function(){	
		$('#mask, .popupInfo').fadeOut(400, function(){
			$('#mask').remove();
		});
		location.reload();
        });
    }); 
    </script>
</head>

<body>

<div class="header">
    
    <div class="searchDiv">
        <form action = 'usersearch.php' method = 'get' name = "search">
        <input type="text" class="tField" placeholder="Search BBB..." name = "keywords"/><input type="submit" class="signInSignUpButton" name = "search"/>
        </form>
    </div>
    <div class="signInRegisterDiv">
		<form method="post" name="signout">
		<a href="userhome.php" class="signInSignUpButton" >Home Page</a><input type="submit" class="signInSignUpButton" value="Sign Out" name="signout"  />
		</form>
    </div>
</div>
<div class="title">
    The Big Bend Bulletin
    <?php
        date_default_timezone_set('America/Chicago');
        $today = date("l, F j, Y");
        htmlentities(printf("<p class = 'todaydate'>%s | Welcome, %s | <a href = 'myprofile.php'>My Profile</a></p>",
                            $today,
							$_SESSION['username']));
    ?>
</div>
<div class="titletrending">
    
        TRENDING ARTICLES:
        <?php #php in trending articles based on likes?>
</div>
<div class="container">
    <div id = "register" class="popupInfo">
        <form method="post" name="createUser" enctype = "multipart/form-data">
        <table class="table">
            <tr><td colspan=2><h1 class="titleSmall">Register</h1></td></tr>
            <tr><td>User Name</td><td><input type="text" name = "newUserName" class = "tField" /></td></tr>
            <tr><td>Password</td><td><input type="password" name = "newPassword" class = "tField" /></td></tr>
            <tr><td>First Name</td><td><input type="text" name = "firstName" class = "tField" /></td></tr>
            <tr><td>Last Name</td><td><input type="text" name = "lastName" class = "tField" /></td></tr>
            <tr><td>Email</td><td><input type="text" name = "email" class = "tField" /></td></tr>
            <tr><td>Picture</td><td><input type="file" name = "picture" /></td></tr>
            <tr><td><input type="submit" name="createUser" class="signInSignUpButton" value="Register" /> </td>
            <td><input type="button" id ="closeReg" class="signInSignUpButton" value="Close" /></td></tr>
        </table>
        </form>
    </div>
    <div id = "login" class="popupInfo">
        <form method="post" name="createUser">
        <table class="table">
            <tr><td colspan=2><h1 class="titleSmall">Sign In</h1></td></tr>
            <tr><td>User Name</td><td><input type="text" name = "username" class = "tField" /></td></tr>
            <tr><td>Password</td><td><input type="password" name = "passwordEntered" class = "tField" /></td></tr>
            <tr><td><input type="submit" name="Login" class="signInSignUpButton" value="Login" /> </td>
            <td><input type="button" id ="closeSignIn" class="signInSignUpButton" value="Close" /></td></tr>
        </table>
        </form>
    </div>

    <?php
		$stmt = $mysqli->prepare("SELECT * from articles");
	   if(!$stmt){
		   printf("Query Prep Failed: %s\n", $mysqli->error);
		   exit;
	   }
	   $stmt->execute();
	   $stmt->bind_result($timestamp, $article_id, $posted_by_id, $posted_by_user, $article_title, $description, $content);
	   
	   while($stmt->fetch()){
			   printf("<p class = \"articleLink\"><a href = \"userviewarticle.php?article_id=%s \" >%s</a><br>%s<br>Posted By: %s on %s</p>\n",
			   htmlspecialchars($article_id),
			   htmlspecialchars($article_title),
			   htmlspecialchars($description),
			   htmlspecialchars($posted_by_user),
			   htmlspecialchars($timestamp));
	   }
	   
	   $stmt->close();
    ?>
</div>



</body>
</html>
