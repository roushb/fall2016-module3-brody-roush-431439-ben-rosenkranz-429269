<?php
require 'databaseAccess.php';
session_start();
if(isset($_SESSION['user_id'])){
}else{
	header('Location: index.php');
}
if(isset($_POST['signout'])){
	session_destroy();
	header('Location: index.php');
}
if(isset($_POST['updateUser'])){
	if($_SESSION['token'] !== $_POST['token']){
			die("ERROR: Request forgery detected. Go away please");
	}else{
		//create username and escape input
			if(!empty($_POST['newUserName'])){
		$user = $mysqli->real_escape_string($_POST['newUserName']);
        }else{
			echo 'Error: Make sure to fill in a username.';
		}
	
	//create password and escape input
    if(!empty($_POST['newPassword'])){
		$pass = $mysqli->real_escape_string($_POST['newPassword']);
        $passEncrypted = crypt($pass,'$1$WQvMDFgI$5.mVOS7V2Q/aB78Mxl13Q1');
       }else{
			echo 'Error: Make sure to fill in a password.';
	}
	//creat first name and escape input
	if(!empty($_POST['firstName'])){
		$first = $mysqli->real_escape_string($_POST['firstName']);
       }else{
			echo 'Error: Make sure to fill in a first name.';
	}
	//create last name and escape input
	if(!empty($_POST['lastName'])){
		$last = $mysqli->real_escape_string($_POST['lastName']);
       }else{
			echo 'Error: Make sure to fill in a last name.';
	}
	//create email and escape input
	if(!empty($_POST['email'])){
		$email = $mysqli->real_escape_string($_POST['email']);;
       }else{
			echo 'Error: Make sure to fill in an email.';
	}
	//create picture link and escape input
	if(!empty($_FILES['picture'])){
        $allowedExt = array('jpeg','jpg','png','gif');
        $file_name = $_FILES['picture']['name'];
        $file_ext = strtolower(end(explode('.', $file_name)));
        $file_temp = $_FILES['picture']['tmp_name'];
        if(in_array($file_ext,$allowedExt)){
            //upload picture
            $file_path = 'imagesfinal/'.substr(md5(time()), 0, 10) . '.' .$file_ext;
            move_uploaded_file($file_temp, $file_path);
        }else{
            echo 'Incorrect file type. Only PNG, jpeg, jpg, and gif are allowed.';
        }
       }else{
			echo 'Error: Make sure to upload a picture.';
		}
		$userid = $_SESSION['user_id'];
		$stmt = $mysqli->prepare("UPDATE users SET username = ?, password = ?, first_name = ?, last_name = ?, picture_link = ?, email = ? WHERE userid = '$userid'");
		
		if(!$stmt){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}else{
		//bind user
		$bind = $stmt->bind_param('ssssss', $user, $passEncrypted, $first, $last, $file_path, $email);
		$execute = $stmt->execute();
		if(!$bind){
			printf("Bind Failed: %s\n", $mysqli->error);
			exit;
		}elseif(!$execute){
			printf("Execute Failed: %s\n", $mysqli->error);
			exit;
		}else{
			$stmt->close();
		}   
		//prepare and insert user into database
		#header('Location: myprofile.php');
	}
	}
}

//Posting an article
if(isset($_POST['postArticle'])){
	$userID = $_SESSION['user_id'];
	$userName = $_SESSION['username'];
	//Get link and prevent attacks
	if(!empty($_POST['newLink'])){
		$link = $_POST['newLink'];
		//Need to escape the query to prevent any attacks
		$safeLink = $mysqli->real_escape_string($_POST['newLink']);
		
		// Test for validity of token
		if($_SESSION['token'] !== $_POST['token']){
			die("ERROR: Request forgery detected. Go away please");
		}
	   }else{
			echo 'Error: Make sure to include a link.';
		}
		
   //Get Article Headline     
	if(!empty($_POST['Headline'])){
		$headline = $_POST['Headline'];
		//Need to escape the query to prevent any attacks
		$safeHeadline = $mysqli->real_escape_string($_POST['Headline']);
		// Test for validity of token
		if($_SESSION['token'] !== $_POST['token']){
			die("ERROR: Request forgery detected. Go away please");
		}
	   }else{
			echo 'Error: Make sure to include a headline.';
		}

	//create description and escape input
	if(!empty($_POST['description'])){
		$description = $_POST['description'];
		//Need to escape the query to prevent any attacks
		$safeDescription = $mysqli->real_escape_string($_POST['description']);
		// Test for validity of token
		if($_SESSION['token'] !== $_POST['token']){
			die("ERROR: Request forgery detected. Go away please");
		}
	}else{
		 echo 'Error: Make sure to include a short description of the article.';
	}
		
	//prepare and insert article into database
		$stmt = $mysqli->prepare("INSERT INTO articles (posted_by_id, posted_by_user, article_title, description, link) values (?,?,?,?,?)");
		if(!$stmt){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}
		//bind user
		$stmt->bind_param('sssss', $userID, $userName, $safeHeadline, $safeDescription, $safeLink);
		$stmt->execute(); 
		$stmt->close(); 
   
}
?>
<!DOCTYPE html>
<html>
<head>
    <link href="news_css.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Cinzel" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">
    <title>The Big Bend Bulletin</title>
    <script src="jquery-3.1.1.min.js"></script>
    <script src="register.js"></script>
    <script>
    $(document).ready(function(){
       $('#update').click(function(){
            $('#register').fadeIn(400);
            $('body').append('<div class="mask" id="mask" ></div>');
            $('#mask').fadeIn(400);
        });
       $('#closeReg').click(function(){	
		$('#mask, .popupInfo').fadeOut(400, function(){
			$('#mask').remove();
		});
		location.reload();
        });
       $('#signIn').click(function(){
            $('#login').fadeIn(400);
            $('body').append('<div class="mask" id="mask" ></div>');
            $('#mask').fadeIn(400);
        });
       $('#closeSignIn').click(function(){	
		$('#mask, .popupInfo').fadeOut(400, function(){
			$('#mask').remove();
		});
		location.reload();
        });
    }); 
    </script>
</head>

<body>

<div class="header">
    
    <div class="searchDiv">
        <form action = 'usersearch.php' method = 'get' name = "search">
        <input type="text" class="tField" placeholder="Search BBB..." name = "keywords"/><input type="submit" class="signInSignUpButton" name = "search"/>
        </form>
    </div>
    <div class="signInRegisterDiv">
      <form method="post" name="signout">
		<a href="userhome.php" class="signInSignUpButton" >Home Page</a><input type="submit" class="signInSignUpButton" value="Sign Out" name="signout"  />
		</form>
    </div>
</div>
<div class="title">
    The Big Bend Bulletin
    <?php
        date_default_timezone_set('America/Chicago');
        $today = date("l, F j, Y");
        htmlentities(printf("<p class = 'todaydate'>%s | Welcome, %s | <a href = 'myprofile.php'>My Profile</a></p>",
                            $today,
							$_SESSION['username']));
    ?>
</div>

<div class="container">
    <div id = "register" class="popupInfo">
        <form method="post" name="createUser" enctype = "multipart/form-data">
        <table class="table">
            <tr><td colspan=2><h1 class="titleSmall">Update Profile</h1></td></tr>
			<tr><td colspan=2>Note: If you do not want to change a field, do not enter anything into that field.</td></tr>
            <tr><td>User Name</td><td><input type="text" name = "newUserName" class = "tField" /></td></tr>
            <tr><td>Password</td><td><input type="password" name = "newPassword" class = "tField" /></td></tr>
            <tr><td>First Name</td><td><input type="text" name = "firstName" class = "tField" /></td></tr>
            <tr><td>Last Name</td><td><input type="text" name = "lastName" class = "tField" /></td></tr>
            <tr><td>Email</td><td><input type="text" name = "email" class = "tField" /></td></tr>
            <tr><td>Picture</td><td><input type="file" name = "picture" /></td></tr>
			<tr><td colspan=2><input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" /></td></tr>
            <tr><td><input type="submit" name="updateUser" class="signInSignUpButton" value="Update" /> </td>
            <td><input type="button" id ="closeReg" class="signInSignUpButton" value="Close" /></td></tr>
        </table>
        </form>
    </div>
    <div id = "login" class="popupInfo">
        <form method="post" name="createUser">
        <table class="table">
            <tr><td colspan=2><h1 class="titleSmall">Sign In</h1></td></tr>
            <tr><td>User Name</td><td><input type="text" name = "username" class = "tField" /></td></tr>
            <tr><td>Password</td><td><input type="password" name = "passwordEntered" class = "tField" /></td></tr>
            <tr><td><input type="submit" name="Login" class="signInSignUpButton" value="Login" /> </td>
            <td><input type="button" id ="closeSignIn" class="signInSignUpButton" value="Close" /></td></tr>
        </table>
        </form>
    </div>
	<h1 class="articleTitle">My Profile</h1>
	<?php
	$currentuserid = $_SESSION['user_id'];
	$stmt = $mysqli->prepare("SELECT * FROM users WHERE (userid = '$currentuserid')");
	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
	 
	$execution = $stmt->execute();
	$binding = $stmt->bind_result($user_joined, $userid, $username, $password, $first_name, $last_name, $picture_link, $email, $active);	
	while($stmt->fetch()){
		printf("<img src = '%s' alt = 'Profile Picture' class = \"profileImage\" /><p class = \"articleLink\">Username: %s<br>Member Since: %s<br>First Name: %s<br>Last Name: %s<br>Email: %s</p>\n",
		htmlspecialchars($picture_link),
		htmlspecialchars($username),
		htmlspecialchars($user_joined),
		htmlspecialchars($first_name),
		htmlspecialchars($last_name),
		htmlspecialchars($email));
	}
	$stmt->close();
	?>

<input type="button" class="signInSignUpButton" value="Update Profile" id="update"  />
<h1 class="articleTitle">Upload an Article</h1>
<form method="post" name="postArticle">
<table class="table">
	<tr><td>Article Title</td><td><input type="text" name = "Headline" class = "tField" /></td></tr>
	<tr><td>Link</td><td><input type="text" name = "newLink" class = "tField" /></td></tr>
	<tr><td>Description</td><td><textarea name = "description" class="description" ></textarea></td></tr>
	<tr><td colspan=2><input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" /></td></tr>
	<tr><td colspan=2><input type="submit" name="postArticle" class="signInSignUpButton" value="Post Article" /> </td></tr>
</table>
</form>
</div>

</body>
</html>
