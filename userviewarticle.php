<?php
require 'databaseAccess.php';
//Start Login Process 
// Use a prepared statement
session_start();
if(isset($_SESSION['user_id'])){
}else{
	header('Location: index.php');
}
if(isset($_POST['signout'])){
	session_destroy();
	header('Location: index.php');
}

    //Posting a comment
if(isset($_POST['comment'])){
	//Get comment and prevent attacks
	if(!empty($_POST['newComment'])){
		$articleid = $_GET['article_id'];
		$comment = $_POST['newComment'];
		//Need to escape the query to prevent any attacks
		$safeComment = $mysqli->real_escape_string($comment);
		// Test for validity of token
		if($_SESSION['token'] !== $_POST['token']){
			die("ERROR: Request forgery detected. Go away please");
		}else{
			
		//prepare and insert comment into database
		   $stmt = $mysqli->prepare("INSERT INTO comments (user_id, user_name, article_id, content) values (?,?,?,?)");
		   if(!$stmt){
			   printf("Query Prep Failed: %s\n", $mysqli->error);
			   exit;
		   }
	   //bind user
		   $bind_param = $stmt->bind_param('isis', $_SESSION['user_id'], $_SESSION['username'], $articleid, $safeComment);
		   $execute = $stmt->execute();
		   if(!$bind_param){
			   printf("Bind param failed", $mysqli->error);
			   exit;
		   }elseif(!$execute){
				printf("Execution failed", $mysqli->error);
			   exit;
		   }
		   $stmt->close();
		}
	}
}
if(isset($_POST['deleteArticle'])){
	$articleid = $_GET['article_id'];
	if($_SESSION['token'] !== $_POST['token']){
			die("ERROR: Request forgery detected. Go away please");
	}else{
		$deleteComments = $mysqli->prepare("DELETE FROM comments WHERE article_id = '$articleid'");
		$deleteComments->execute();
		$deleteComments->close();
		
		$deleteArticle = $mysqli->prepare("DELETE FROM articles WHERE article_id = '$articleid'");
		
		if(!$deleteArticle){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}else{
			$deleteArticle->execute();
			$deleteArticle->close();
			header('location: userhome.php');
		}
	}	
}
if(isset($_POST['deleteComment'])){
	$commentid = $_POST['commentid'];
	if($_SESSION['token'] !== $_POST['token']){
			die("ERROR: Request forgery detected. Go away please");
	}else{
		$deleteComment = $mysqli->prepare("DELETE FROM comments WHERE comment_id = '$commentid'");
		
		if(!$deleteComment){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}else{
			$deleteComment->execute();
			$deleteComment->close();
		}
	}	
}
if(isset($_POST['editArticle'])){
	$articleid = $_GET['article_id'];
	if($_SESSION['token'] !== $_POST['token']){
			die("ERROR: Request forgery detected. Go away please");
	}else{
		$articleTitle = $mysqli->real_escape_string($_POST['articlename']);
		$articleDesc = $mysqli->real_escape_string($_POST['description']);
		$articleLink = $mysqli->real_escape_string($_POST['link']);
		
		$updateArticle = $mysqli->prepare("UPDATE articles SET article_title = '$articleTitle', description = '$articleDesc', link = '$articleLink' WHERE article_id = '$articleid'");
		
		if(!$updateArticle){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}else{
			$updateArticle->execute();
			$updateArticle->close();
		}	
	
	}	
}
if(isset($_POST['editComment'])){
	if($_SESSION['token'] !== $_POST['token']){
			die("ERROR: Request forgery detected. Go away please");
	}else{
		$commentid = $_POST['commentid'];
		$newContent = $mysqli->real_escape_string($_POST['updatedComment']);
		$updateComment = $mysqli->prepare("UPDATE comments SET content = '$newContent' WHERE comment_id = '$commentid'");
		
		if(!$updateComment){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}else{
			$execution3 = $updateComment->execute();
			if(!$execution3){
				printf("Execution Failed: %s\n", $mysqli->error);
				exit;
			}
			$updateComment->close();
		}	
	
	}
}

?>
<!DOCTYPE html>
<html>
<head>
    <link href="news_css.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Cinzel" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">
    <title>The Big Bend Bulletin</title>
    <script src="jquery-3.1.1.min.js"></script>
    <script src="register.js"></script>
    <script>
    $(document).ready(function(){
       $('#signUp').click(function(){
            $('#register').fadeIn(400);
            $('body').append('<div class="mask" id="mask" ></div>');
            $('#mask').fadeIn(400);
        });
       $('#closeReg').click(function(){	
		$('#mask, .popupInfo').fadeOut(400, function(){
			$('#mask').remove();
		});
		location.reload();
        });
       
       $('#closeEditArticle').click(function(){	
		$('#mask, .popupInfo').fadeOut(400, function(){
			$('#mask').remove();
		});
		location.reload();
        });
	   $('#closeEditComment').click(function(){	
		$('#mask, .popupInfo').fadeOut(400, function(){
			$('#mask').remove();
		});
		location.reload();
        });
    }); 
    </script>
</head>

<body>

<div class="header">
    
    <div class="searchDiv">
        <form action = 'usersearch.php' method = 'get' name = "search">
        <input type="text" class="tField" placeholder="Search BBB..." name = "keywords"/><input type="submit" class="signInSignUpButton" name = "search"/>
        </form>
    </div>
    <div class="signInRegisterDiv">
      <form method="post" name="signout">
		<a href="userhome.php" class="signInSignUpButton" >Home Page</a><input type="submit" class="signInSignUpButton" value="Sign Out" name="signout"  />
	</form>
    </div>
</div>
<div class="title">
    The Big Bend Bulletin
    <?php
        date_default_timezone_set('America/Chicago');
        $today = date("l, F j, Y");
        htmlentities(printf("<p class = 'todaydate'>%s | Welcome, %s | <a href = 'myprofile.php'>My Profile</a></p>",
                            $today,
							$_SESSION['username']));
    ?>
</div>

<div class="container">
    <div id = "editComment" class="popupInfo">
        <form method="post" name="editComment">
        <table class="table">
            <tr><td colspan=2><h1 class="titleSmall">Edit Comment</h1></td></tr>
            <tr><td>Comment</td><td><textarea name = "updatedComment"></textarea></td></tr>
			<tr><td colspan=2><input type="hidden" name = "token" value = "<?php echo $_SESSION['token']?>"><input type="text" name = "commentidhidden" /></td></tr>
            <tr><td><input type="submit" name="editComment" class="signInSignUpButton" value="Update Comment" /> </td>
            <td><input type="button" id ="closeEditComment" class="signInSignUpButton" value="Close" /></td></tr>
        </table>
        </form>
    </div>
    <div id = "editArticle" class="popupInfo">
        <form method="post" name="editArticle">
        <table class="table">
            <tr><td colspan=2><h1 class="titleSmall">Edit Article</h1></td></tr>
            <tr><td>Article Name</td><td><input type="text" name = "articlename" class = "tField" /></td></tr>
            <tr><td>Description</td><td><input type="text" name = "description" class = "tField" /></td></tr>
			<tr><td>Link</td><td><input type="text" name = "link" class = "tField" /></td></tr>
			<tr><td colspan=2><input type="hidden" name = "token" value = "<?php echo $_SESSION['token']?>"></td></tr>
            <tr><td><input type="submit" name="editArticle" class="signInSignUpButton" value="Edit Article" /> </td>
            <td><input type="button" id ="closeEditArticle" class="signInSignUpButton" value="Close" /></td></tr>
        </table>
        </form>
    </div>

    <?php
	$articleid = $_GET['article_id'];
    $stmt = $mysqli->prepare("SELECT * FROM articles WHERE article_id = '$articleid'");
    if(!$stmt){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }
    $stmt->execute();
    $stmt->bind_result($timestamp, $article_id, $posted_by_id, $posted_by_user, $article_title, $description, $link);
    
    while($stmt->fetch()){
            printf("<h1 class = \"articleTitle\">%s</h1><p class=\"articleInfo\">%s<br>Posted By: %s on %s<br><br><a href = \"%s\">%s</a></p>\n",
            htmlspecialchars($article_title),
            htmlspecialchars($description),
            htmlspecialchars($posted_by_user),
            htmlspecialchars($timestamp),
			htmlspecialchars($link),
			htmlspecialchars($link));
    }
    
    $stmt->close();
	$articleid = $_GET['article_id'];
	$stmt = $mysqli->prepare("SELECT posted_by_id FROM articles WHERE article_id = '$articleid'");
	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
	$execute = $stmt->execute();
	$stmt->bind_result($posted_by_id);
	if($posted_by_id == $_SESSION['user_id']){
		printf("<form method=\"post\" name=\"deleteArticle\"><input type=\"hidden\" name=\"token\" value=\"%s\"/><input type=\"submit\" name =\"deleteArticle\" class=\"signInSignUpButton\" value=\"Delete Article\" />\n",
            htmlspecialchars($_SESSION['token']));
		printf("<input type=\"button\" id =\"editArticleButton\" class=\"signInSignUpButton\" value=\"Edit Article\" /></form>\n",
            htmlspecialchars($_SESSION['token']));
	}
	$stmt->close();
	?>
	<h1 class="articleTitle">Comments:</h1>
	<form method="post" name="comment">
		<textarea name="newComment" cols="40" rows="3" placeholder="Write a comment here..."></textarea><br>
		<input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
		<input type="submit" name = "comment" class="signInSignUpButton" value="Comment" />
	</form>
	<?php
	$stmtComment = $mysqli->prepare("SELECT * FROM comments WHERE article_id = '$articleid'");
    if(!$stmtComment){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }
    $stmtComment->execute();
    $stmtComment->bind_result($timestamp, $comment_id, $posted_by_id, $posted_by_user, $article_id,$content);
    
    while($stmtComment->fetch()){
		if($posted_by_id == $_SESSION['user_id']){
            printf("<form method=\"post\" name=\"deleteComment\"><p class=\"comments\"><b>%s</b> %s<br><span class =\"commentTimestamp\">Posted On: %s</span>\n",
            htmlspecialchars($posted_by_user),
            htmlspecialchars($content),
            htmlspecialchars($timestamp));
			printf("<br><input type=\"hidden\" name=\"token\" value=\"%s\"/>
				   <input type=\"hidden\" name=\"commentid\" value=\"%s\"/>
				   <input type=\"submit\" name =\"deleteComment\" class=\"signInSignUpButton\" value=\"Delete Comment\" /></p></form>
				   <form method=\"post\" name=\"editComment\" class = \"editComment\">
				   <p class=\"comments\"><input type=\"hidden\" name=\"token\" value=\"%s\"/>
				   <input type=\"hidden\" name=\"commentid\" value=\"%s\"/>
				   <input type=\"text\" name=\"updatedComment\" class = \"tField\" placeholder=\"Edit above comment...\"/>
				   <input type=\"submit\" name =\"editComment\" class=\"signInSignUpButton\" value=\"Edit Comment\" /></p></form>\n",
				htmlspecialchars($_SESSION['token']),
				htmlspecialchars($comment_id),
				htmlspecialchars($_SESSION['token']),
				htmlspecialchars($comment_id));
		}else{
			printf("<p class=\"comments\"><b>%s</b> %s<br><span class =\"commentTimestamp\">Posted On: %s</span></p>\n",
            htmlspecialchars($posted_by_user),
            htmlspecialchars($content),
            htmlspecialchars($timestamp));
		}	
    }
	
	
    ?>
</div>



</body>
</html>
